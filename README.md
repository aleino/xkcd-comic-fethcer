# XKCD Comic Fethcer

Fetches a random XKCD comic with metadata and saves into `data` directory.

To save the disk space, application only keeps the latest (default 2) comics stored.

Application uses [XKCD JSON API](https://xkcd.com/json.html) to get a valid random comic ID and metadata.

## Requirements

- Python 3.7
- Pipenv

## Install

```sh
pipenv --three
pipenv install
```

## Usage

To fecth a a random comic with metadata run:

```sh
pipenv run python src/fetch_comic.py
```

## Logging

Application logs function executions into `stdout` and `stderr`. You can set the logging level with `LOG_LEVEL` environment variable.

## Configuration

You can set the number of saved latest comics by settting `N_COMICS_TO_KEEP` environment variable.

## Notes

### HTTPS Requests

Application makes three HTTP requests:

- One to get a valid random comic ID.
- One to fetch the metadata.
- One to fetch the actual comic.

If HTTP requests are considered expensive, a random number generator could be used to generate the comic ID, for example `comic_id = round(random() * 2300)`
