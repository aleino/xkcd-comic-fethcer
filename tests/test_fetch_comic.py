from src.fetch_comic import fetch_random_comic_id


def test_random_comic_id_is_valid_number():
    comic_id = fetch_random_comic_id()
    assert type(comic_id) is int
    assert 0 < comic_id < 10e5
