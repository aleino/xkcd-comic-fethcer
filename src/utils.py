import json


from logger import logme, create_logger
logger = create_logger(__name__)


@logme()
def write_json(data, path):
    with open(path, 'w') as outfile:
        json.dump(data, outfile)


@logme()
def read_last_comic_id(path):
    try:
        with open(path, 'r') as infile:
            return int(infile.readline())
    except FileNotFoundError as ex:
        logger.warn('Last comic ID file does not yet exist')
        return None
    except AttributeError as ex:
        logger.error('Invalid last comic ID file')
        raise


@logme()
def save_last_comic_id(comic_id, path):
    with open(path, 'w') as outfile:
        outfile.write(comic_id)


@logme()
def save_file(data, path, mode='w'):
    with open(path, mode) as outfile:
        outfile.write(data)
