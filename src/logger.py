import logging
import os
from functools import wraps


def create_logger(name):
    """
    Creates a logging object and returns it
    """
    logger = logging.getLogger(name)
    logger.setLevel(os.environ.get('LOG_LEVEL', logging.INFO))

    # create the logging file handler
    ch = logging.StreamHandler()

    fmt = '%(asctime)s - %(levelname)s - %(message)s'
    # fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(fmt)
    ch.setFormatter(formatter)

    # add handler to logger object
    logger.addHandler(ch)
    return logger


def logme():
    def wrap(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            function_name = function.__name__
            logger = create_logger(function_name)
            logger.info('START {}'.format(function_name))
            try:
                response = function(*args, **kwargs)
            except Exception as error:
                logger.error('ERROR while executing  {}'.format(function_name))
                raise error
            finally:
                logger.info('END {}'.format(function_name))

            return response
        return wrapper
    return wrap
