import os
import glob
import time

import requests

from utils import write_json
from utils import save_file
from utils import read_last_comic_id
from utils import save_last_comic_id
from logger import logme, create_logger

XKCD_BASE_URL = 'http://xkcd.com'
RANDOM_XKCD_URL = 'https://c.xkcd.com/random/comic/'
N_COMICS_TO_KEEP = os.environ.get('N_COMICS_TO_KEEP', 2)

DATA_DIR = './data/'
IMG_DIR = os.path.join(DATA_DIR, 'img')
METADATA_DIR = os.path.join(DATA_DIR, 'metadata')
LAST_COMIC_ID_PATH = os.path.join(DATA_DIR, 'last_comic_id.txt')

logger = create_logger(__name__)


@logme()
def fetch_random_comic_id():
    """Use XKCD API to fetch a valid random comic id"""
    res = requests.get(RANDOM_XKCD_URL, allow_redirects=False)
    res.raise_for_status()
    location = res.headers['Location']
    comic_id = int(location.split('/')[-2])  # https://xkcd.com/1234/
    logger.info('Fetched comic id {}'.format(comic_id))

    return comic_id


@logme()
def fetch_comic_metadata(comic_id):
    """XKCD JSON interface https://xkcd.com/json.html"""
    url = '{}/{}/info.0.json'.format(XKCD_BASE_URL, comic_id)
    res = requests.get(url)
    res.raise_for_status()
    comic_metadata = res.json()
    logger.info(comic_metadata)

    return comic_metadata


@logme()
def fecth_image(url):
    res = requests.get(url, stream=True)
    res.raise_for_status()
    img = res.content

    return img


@logme()
def remove_old_comics(n_comic_to_keep=2):
    files = glob.glob(os.path.join(IMG_DIR, '*'))
    sorted_files = sorted(files, key=lambda file: os.path.getctime(file), reverse=True)

    for file in sorted_files:
        logger.debug("{} - {}".format(time.ctime(os.path.getctime(file)), file))

    files_to_remove = sorted_files[n_comic_to_keep:]
    for file in files_to_remove:
        logger.info("Remove {} - {}".format(time.ctime(os.path.getctime(file)), file))
        os.remove(file)


def fetch_random_comic():
    last_comic_id = read_last_comic_id(LAST_COMIC_ID_PATH)
    # Prevent fetching same comic twice in a row
    while True:
        comic_id = fetch_random_comic_id()
        if comic_id != last_comic_id:
            break

    metadata = fetch_comic_metadata(comic_id)
    img = fecth_image(metadata['img'])

    metadata_filename = '{}-{}.json'.format(comic_id, metadata['safe_title'])
    metadata_path = os.path.join(METADATA_DIR, metadata_filename)
    write_json(metadata, metadata_path)

    # metadata['img'] looks like https://imgs.xkcd.com/comics/terminology.png
    img_filename = '{}-{}'.format(comic_id, metadata['img'].split('/')[-1])
    img_path = os.path.join(IMG_DIR, img_filename)
    save_file(img, img_path, mode='wb')

    save_last_comic_id(str(comic_id), LAST_COMIC_ID_PATH)
    remove_old_comics(N_COMICS_TO_KEEP)


if __name__ == "__main__":
    fetch_random_comic()
